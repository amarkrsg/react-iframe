import Link from "next/link";

const Header = () => {
    return(
        <header className="header">
            <div className="logo">
                <a>Logo</a>
            </div>
            <div className="menu">
                <ul className="menu-bar">
                    <li>
                        Menu 1
                    </li>
                    <li>
                        Menu 2
                    </li>
                    <li>
                        Menu 3
                    </li>
                </ul>
            </div>
        </header>
    )
}

export default Header;